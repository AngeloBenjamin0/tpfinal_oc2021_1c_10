/*para probar en la terminal: echo "msj a codificar;3;c" | ./leerGuardar > out.txt ; cat out.txt*/
/*----------------------------------------------------------*/
.data
mensaje: .ascii "Ingrese un mensaje, clave y opcion, por favor: "
longitudMensaje = . - mensaje

mensaje2: .ascii "Ud. ha ingresado: "
longitudMensaje2 = . - mensaje2

enter: .ascii "\n"

inputUsuario:  .asciz "                                                                                                   "
longitudInputUsuario = . - inputUsuario

msjInput: .asciz "                                                                                                         "
longitudMsjInput = . - msjInput

clave: .word /*Ya guardado como numero decimal y no en ascii*/ 

opcion: .asciz " " /*Va el char de opcion, puede ser 'c' o 'd'*/ 
longitudOpcion = . - opcion

.text
/*----------------------------------------------------------*/
leerMensaje:
.fnstart
    /*Parametros inputs: no tiene*/
    /*Parametros output: 
    r0=char leido*/
        
    mov r7, #3    /*Lectura x teclado*/ 
    mov r0, #0      /*Ingreso de cadena*/ 

    ldr r2, =longitudInputUsuario /*Leer # caracteres */
    ldr r1, =inputUsuario /*donde se guarda la cadena ingresada */
    swi 0     
    ldr r0, [r1] 

    bx lr /*volvemos a donde nos llamaron */
.fnend
/*----------------------------------------------------------*/
/*extrae msj, clave y opción todo de una, y guarda en las etiquetas */
extraer:
    .fnstart
      ldr r1, =inputUsuario /*apunta al input para empezar a recorrerlo*/
      ldr r3, =msjInput /*apunta a la etiqueta donde guarda el mensaje limpio*/

    extraerMensaje:
        ldrb r2, [r1] /*cargo en r2 solo el byte del ascii*/
        cmp r2,#';' /*termina mensaje y empieza la clave*/
        beq extraerClave /*salta para apuntar con r3 a clave y guardar ahi*/
        strb r2, [r3] /*carga en r3 el contenido de r2 de a un byte, caracter*/
        add r1, #1 /*sigo recorriendo */
        add r3, #1 /*para avanzar y no sobreecribir */
        b extraerMensaje

    extraerClave:
        ldr r3, =clave
        add r1, #1 /*avanzar, para el char sig al ';' */
        ldrb r2, [r1] /*carga la clave*/
        sub r2, #0x30 /*para pasar a decimal*/
        str r2, [r3] /*@carga en r3 el contenido de r2*/
        add r1, #2 /*pasar del ; a la opcion directamente */
    
    extraerOpcion:
        ldr r3, =opcion
        ldrb r2, [r1] /*carga la opcion*/
        strb r2, [r3] /*guarda en r3 el contenido de r2 de a un byte, caracter
        @sale directo, despues de la opcion ya no hay nada*/
        bxeq lr /*salta a donde lo llamaron, sale de la subrutina*/

    .fnend
/*----------------------------------------------------------*/
imprimirString:
    .fnstart
      /*Parametros inputs:
      r1=puntero al string que queremos imprimir
      r2=longitud de lo que queremos imprimir*/
      mov r7, #4 /* Salida por pantalla  */
      mov r0, #1      /*Indicamos a SWI que sera una cadena */          
      swi 0    /*SWI, Software interrup*/
      bx lr /*salimos de la funcion mifuncion*/
    .fnend
/*----------------------------------------------------------*/
newLine:
    .fnstart
      push {lr}
      mov r2, #1 /*Tamaño de la cadena*/
      ldr r1, =enter   /*Cargamos en r1 la direccion del mensaje*/
      bl imprimirString
      pop {lr}
      bx lr /*salimos de la funcion mifuncion */           
    .fnend
/*----------------------------------------------------------*/

.global main 
main:
    
    /*Imprimir por pantalla msj que pide input al usuario*/
    ldr r2, =longitudMensaje /*Tamaño de la cadena*/
    ldr r1, =mensaje   /*Cargamos en r1 la direccion del mensaje*/
    bl imprimirString
    
    /*leemos msj*/
    bl leerMensaje

    /*Imprimir por pantalla "Ud. ha ingresado:"*/
    ldr r2, =longitudMensaje2 /*Tamaño de la cadena*/
    ldr r1, =mensaje2   /*Cargamos en r1 la direccion del mensaje*/
    bl imprimirString

    
    ldr r2, =longitudInputUsuario /*Leer # caracteres*/
    ldr r1, =inputUsuario    /*Cargamos en r1 la direccion del mensaje*/
    bl imprimirString

    bl extraer /*extrae msj, clave y opcion y guarda en las correspondientes etiquetas*/
    bl newLine /*Salto de linea, mas prolijo en la terminal */ 

    ldr r2, =longitudMsjInput /*Leer # caracteres*/
    ldr r1, =msjInput    /*Cargamos en r1 la direccion del mensaje*/
    bl imprimirString

    bl newLine

    /*lo de mostrar la opcion no va a quedar, es para probar*/
    ldr r2, =longitudOpcion /*Leer # caracteres */ 
    ldr r1, =opcion    /*Cargamos en r1 la direccion del mensaje*/
    bl imprimirString

    bl newLine    /* Para acomodar el prompt al terminar el programa*/     
    mov r7, #1    
    swi 0