/*codificar mayusculas*/
.data
texto: .asciz "Z B"
codificado: .asciz "   "
corrimiento: .word 3
.text
.global main
main:
    ldr r3,=texto
    ldr r6,=corrimiento
    ldr r6,[r6]
    ldr r0,=codificado
    mov r1,#65
ciclo:
    ldrb r5,[r3]
    cmp r5,#0x20
    beq cargarEspacio /*no hay que codificar espacios*/
    cmp r5,#0
    beq fin
numerador:
    add r5,r6
    sub r5,r1
    cmp r5,#26
    bgt modulo
    b cargarEnString
modulo:
    sub r5,#26 /*r5=resto o modulo */
    cmp r5,#26
    bgt modulo
    b cargarEnString
cargarEnString:
    add r5,r1
    str r5,[r0]
    add r3,#1
    add r0,#1
    b ciclo
cargarEspacio:
    str r5,[r0]
    add r3,#1
    add r0,#1
    b ciclo
fin:
 	mov r7, #1
	swi 0