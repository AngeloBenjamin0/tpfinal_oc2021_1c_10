/*Guarda el primer caracter en la primera direccion de output,
el puntero se mueve a la segunda direccion de output y guarda ahi
el segundo caracter */
.data
text: .asciz "ab"
output: .asciz "   "
.text
.global main
main:
    mov r0,#0
    mov r1,#0
    mov r2,#0
    ldr r0,=text
    ldr r3,=output
ciclo:
    ldrb r1,[r0]
    cmp r1,#0
    beq final
    strb r1, [r3]
    add r3,#1
    add r0,#1
    b ciclo
final:
    mov r7,#1
    swi 0