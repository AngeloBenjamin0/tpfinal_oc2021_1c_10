/*Versión final, 24/06, 14:05 hs*/ 
/*----------------------------------------------------------*/
.data
mensaje: .ascii "Ingrese separando con ';' mensaje (sin tildes y ñ), clave (1 a 999) opción (c/d). (Lo que ponga luego de 'c' o 'C' será ignorado.) \nPara desencriptar un mensaje cifrado ingrese ';' luego de la opción y '0' o '1', sin espacios, para indicar bit de paridad (lo que ponga después de '0' o '1' será ignorado). \nSi desea desencriptar un mensaje sin tener la clave, ingrese el mensaje+';'+pista (la pista debe ser UNA PALABRA entera): "
longitudMensaje = . - mensaje

mensaje1: .ascii "\nUd. ha ingresado: "
longitudMensaje1 = . - mensaje1

mensaje2: .ascii "Mensaje ingresado: "
longitudMensaje2 = . - mensaje2

mensaje3: .ascii "Clave ingresada: "
longitudMensaje3 = . - mensaje3

mensaje4: .ascii "Opción ingresada: "
longitudMensaje4 = . - mensaje4

mensaje5: .ascii "Cantidad de caracteres codificados/decodificados: "
longitudMensaje5 = . - mensaje5

mensaje6: .ascii "\nMensaje codificado/decodificado: "
longitudMensaje6 = . - mensaje6

mensaje7: .ascii "Bit de paridad: "
longitudMensaje7 = . - mensaje7

mensaje8: .ascii "Pista ingresada: "
longitudMensaje8 = . - mensaje8

mensaje9: .ascii "Mensaje: "
longitudMensaje9 = . - mensaje9

mensajeErrorOpcion: .ascii "La opción es inválida, debe ser c, C, d o D. Intente nuevamente."
longMensErrorOpcion = . - mensajeErrorOpcion

mensajeErrorInput: .ascii "Entrada INCORRECTA. Chequear: separación con ';', recordar no poner espacios entre ';clave;opción'. \n(En el mensaje puede haber signos de puntuación (excepto ';'), espacios, etc., pero en clave sólo números en base 10. En opción sólo un carácter y, si corresponde, en bit de paridad sólo '0' o '1'.) \nPara usar una pista recuerde ingresar un único ';'."
longMensErrorInput = . - mensajeErrorInput

mensajeErrorBitDeParidad: .ascii "El bit de paridad no corresponde, intente nuevamente."
longitudMensajeErrorBitDeParidad = . -mensajeErrorBitDeParidad   

mensajeErrorPista: .ascii "La pista es INCORRECTA. Intente nuevamente. Recuerde que debe ser una palabra sin tildes, ni números ni palabras incompletas."
longitudMensajeErrorPista = . - mensajeErrorPista

mensajeDesplazamiento: .ascii "Desplazamiento: -"
longitudMensajeDesplazamiento = . - mensajeDesplazamiento

enter: .ascii "\n"

inputUsuario:  .asciz "                                                                                                                              "
longitudInputUsuario = . - inputUsuario

msjInput: .asciz "                                                                                                                                 "
longitudMsjInput = . - msjInput

clave: .asciz "                                               " 
longitudClave = . - clave

claveDecimal: .space 4

cantCaracteres: .space 4

cantEnAscii: .asciz "       "
longitudCantEnAscii = . - cantEnAscii

bitDeParidad: .asciz " "
longitudBitDeParidad = . - bitDeParidad

opcion: .asciz " " /*Va el char de opcion, puede ser 'c', 'C' o 'd', 'D' */ 
longitudOpcion = . - opcion

bitParidadUsuario: .asciz " "
longitudBitParidadUsuario = . - bitParidadUsuario

msjCodificado: .asciz "                                                                                                         "
longitudMsjCodificado = . - msjCodificado

pista: .asciz "                                "
longitudPista = . - pista

corrimiento: .space 4

opcionD: .asciz "d"

msjDecodificado: .asciz "                                                                                                         "
longitudMsjDecodificado = . - msjDecodificado

.text
/*----------------------------------------------------------*/
leerMensaje:
.fnstart
    /*Parametros inputs: no tiene*/
    /*Parametros output: 
    r0=char leido*/
        
    mov r7, #3    /*Lectura x teclado*/ 
    mov r0, #0      /*Ingreso de cadena*/ 

    ldr r2, =longitudInputUsuario 
    ldr r1, =inputUsuario 
    swi 0     
    ldr r0, [r1] 

    bx lr 
.fnend
/*----------------------------------------------------------*/
extraer:
    .fnstart
      ldr r1, =inputUsuario 
      ldr r3, =msjInput
      ldr r0, =longitudInputUsuario
      eor r4, r4 /*para controlar input*/

    extraerMensaje:
        ldrb r2, [r1] 
        cmp r2,#';' /*termina mensaje y empieza la clave*/
        beq extraerClave /*salta para apuntar con r3 a clave y guardar ahi*/
        add r4, #1
        cmp r4, r0
        beq errorInput
        strb r2, [r3] /*carga en r3 el contenido de r2 de a un byte, caracter*/
        add r1, #1 /*sigo recorriendo */
        add r3, #1 /*para avanzar y no sobreecribir */
        b extraerMensaje

    extraerClave:
        ldr r3, =clave
        add r1, #1 /*avanzar, para el char sig al ';' */
    
        bucle:
        ldrb r2, [r1] /*carga la clave en r2*/ 
        cmp r2,#';' 
        beq extraerOpcion 
        cmp r2, #'0'
        blt errorInput 
        cmp r2, #'9'
        bhi errorInput
        str r2, [r3] 
        add r1, #1 
        add r3, #1 
        b bucle
    
    extraerOpcion:
        add r1, #1 /*pasar del ; a la opcion*/
        ldr r3, =opcion
        ldrb r2, [r1] /*carga la opcion*/
        strb r2, [r3]
        cmp r2, #'d'
        beq bitParidad
        cmp r2, #'D'
        beq bitParidad
        cmp r2, #'c' 
        bxeq lr
        cmp r2, #'C' 
        bxeq lr
        b errorInput

    bitParidad:
        add r1, #1
        ldrb r2, [r1]
        cmp r2, #';'
        bne errorInput
        add r1, #1
        ldrb r2, [r1]
        cmp r2, #'0'
        beq cargarBit
        cmp r2, #'1'
        bne errorBit
        cargarBit:
        ldr r3, =bitParidadUsuario
        strb r2, [r3]
        bx lr
 
    errorInput:
        bl newLine
        ldr r2, =longMensErrorInput
        ldr r1, =mensajeErrorInput  
        bl imprimirString
        bal fin
    errorBit:
        bl newLine
        ldr r2, =longitudMensajeErrorBitDeParidad
        ldr r1, =mensajeErrorBitDeParidad  
        bl imprimirString
        bal fin
    .fnend
/*----------------------------------------------------------*/
/*a partir de clave, que guarda en ascii, pasa a nro decimal la clave */
claveAdecimal:
    .fnstart
        /*Input:
        r1: apunta a clave puntero al vector, para recorrer
        r3: apunta a claveDecimal puntero al lugar de memoria donde guardar el numero
        r4:=10 para los pesos 
        r0:=0 inicializa en cero*/

        bucleNum:
        ldrb r2, [r1] /*carga un char (nro en ascii) de la clave en r2*/
        cmp r2, #0 
        beq cargarNum
        sub r2, #0x30 /*para pasar a decimal*/
        mul r0, r4 /*r0 acumula los resultados, inicia en cero, luego se mult por 10 segun la cant de digitos */
        add r0, r2 /*suma y guarda, para despues mult por 10 si hay mas dig y volver a sumar and so on */
        add r1, #1 /*avanza*/
        b bucleNum

        cargarNum:
        str r0, [r3] /*en r0 fuimos acumlando el nro, r3 apunta a claveDecimal, donde se guarda el nro*/
        bx lr
    .fnend
/*----------------------------------------------------------*/
chequearOpcion:
    .fnstart    
        cmp r1, #'d'
        bxeq lr
        cmp r1, #'D'
        bxeq lr
        cmp r1, #'c'
        bxeq lr
        cmp r1, #'C'
        bxeq lr 

        opcionIncorrecta:
        bl newLine
        ldr r2, =longMensErrorOpcion
        ldr r1, =mensajeErrorOpcion  
        bl imprimirString
        bal fin 
    .fnend
/*----------------------------------------------------------*/
imprimirString:
    .fnstart
      /*Parametros inputs:
      r1=puntero al string que queremos imprimir
      r2=longitud de lo que queremos imprimir*/
      mov r7, #4 /* Salida por pantalla  */
      mov r0, #1      /*Indicamos a SWI que sera una cadena */          
      swi 0    
      bx lr 
    .fnend
/*----------------------------------------------------------*/
newLine:
    .fnstart
      push {lr}
      mov r2, #1 /*Tamaño de la cadena*/
      ldr r1, =enter   /*Cargamos en r1 la direccion del mensaje*/
      bl imprimirString
      pop {lr}
      bx lr           
    .fnend
/*----------------------------------------------------------*/
filtrarLetras:
    /*r1: apunta a msjInput 
    r0: pro a msjCodificado
    r2: carga la clave en decimal
    r5: contador
    output: msj codificado, cantidad de caracteres en r5*/
    .fnstart
        push {lr}
        filtrar:
        ldrb r3, [r1]/*carga en r3 el byte de la primera dirección de r1(texto) */
        cmp r3, #0
        beq termina
        cmp r3, #'z'
        bhi cargar
        cmp r3, #'a'
        movcs r6, #97
        addcs r5, #1
        blcs minusculaMayuscula /* >= */
        cmp r4, #1
        beq adelanta
        cmp r3, #'Z'
        bhi cargar  /* > */
        cmp r3, #'A'
        movcs r6, #65
        addcs r5, #1
        blcs minusculaMayuscula /* r3>=A */
        cmp r4, #1
        beq adelanta
        cmp r3, #'9'
        bhi cargar /*r3>9 */
        cmp r3, #'0'
        bcc cargar
        mov r6, #48
        add r5, #1
        bl chequeoOpcionNumero
        cmp r4,#1
        beq adelanta

        cargar:
        str r3, [r0]

        adelanta:
        mov r4, #0
        add r1, #1
        add r0, #1
        b filtrar

        termina:
        pop {lr}
        bx lr
    .fnend
/*--------------------------- */
/*subrutina minuscula mayuscula, mov r4, #1 al volver, controlador */
minusculaMayuscula:
    /*input: char en r3,
    clave en r2, opcion en r5
    En r6 está el corrimiento, para mayuscula o minucula 
    output: no retorna, deriva*/
    .fnstart
        push {lr}
        push {r5} 
        ldr r5, =opcion
        ldrb r5, [r5]
        cmp r5, #'d'
        bleq decodificar
        cmp r5, #'D'
        bleq decodificar
        cmp r5, #'c'
        bleq codificar
        cmp r5, #'C'
        bleq codificar

        volver:
        pop {r5}
        pop {lr}
        mov r4, #1
        bx lr
    .fnend
/*----------------------------- */
chequeoOpcionNumero:
    /*input: char en r3,
    clave en r2, opcion en r5
    En r6 está el corrimiento, para mayuscula o minucula 
    output: no retorna, deriva*/
    .fnstart
        push {lr}
        push {r5} 
        ldr r5, =opcion
        ldrb r5, [r5]
        cmp r5, #'d'
        bleq decodificarNum
        cmp r5, #'D'
        bleq decodificarNum
        cmp r5, #'c'
        bleq codificarNum
        cmp r5, #'C'
        bleq codificarNum

        volver1:
        pop {r5}
        pop {lr}
        mov r4, #1
        bx lr
    .fnend
/*----------------------------- */
    /*input: char en r3,
    clave en r2
    En r6 está el corrimiento, para mayuscula(65) o minucula (97)
    output: guarda msj en r0*/

codificar:
    .fnstart
        add r3, r2
        sub r3, r6 /*para trabajar con el alfabeto desde 1 a 26*/
        cmp r3, #25 
        bhi modulo /*corrige si r3 >= 26*/
        b cargarCodificado

        modulo:
        sub r3, #26 
        cmp r3, #25
        bhi modulo /*dividir mientras resto>=26 */
        b cargarCodificado

        cargarCodificado:
        add r3, r6 /*para volver a ascii*/
        str r3, [r0]
        bx lr
    .fnend

/*---------------------------- */
/*mismo input y output que codificar */
decodificar:
    .fnstart
        sub r3, r2
        sub r3, r6 
        cmp r3, #0 
        blt sumar /*corrige si r3 < 1, less than porque puede ser negativo*/
        b cargarDecodificado

        sumar:
        add r3, #26 
        cmp r3, #0 
        blt sumar /*sumar mientras resto<1 */
        b cargarDecodificado

        cargarDecodificado:
        add r3, r6 
        str r3, [r0]
        bx lr
    .fnend
/*----------------------------------------------------------------- */
codificarNum:
    .fnstart
        add r3, r2
        sub r3, r6 /*para trabajar con los numeros del 1 al 9*/
        cmp r3, #9 
        bhi modulo1 /*corrige si r3 >= 9*/
        b cargarCodificado1

        modulo1:
        sub r3, #10
        cmp r3, #9
        bhi modulo1 /*dividir mientras resto>=9 */
        b cargarCodificado1

        cargarCodificado1:
        add r3, r6 /*para volver a ascii*/
        str r3, [r0]
        bx lr
    .fnend
/*----------------------------------------------------------------- */
decodificarNum:
    .fnstart
        sub r3, r2
        sub r3, r6 
        cmp r3, #0 
        blt sumar1 /*corrige si r3 < 1, less than porque puede ser negativo*/
        b cargarDecodificado1

        sumar1:
        add r3, #10 
        cmp r3, #0 
        blt sumar1 /*sumar mientras resto<1 */
        b cargarDecodificado1

        cargarDecodificado1:
        add r3, r6 
        str r3, [r0]
        bx lr
    .fnend
/*----------------------------------------------------------------- */
enteroAascii:
    /*input: r4:=cantCaracteres
    r5 puntero a cantEnAscii
    output: cantidad de caracteres en ascii, cantEnAscii
    r0 --> unidades
    r1 --> decenas
    r2 --> centenas
    r3 --> contador*/
    .fnstart
    eor r0, r0
    eor r1, r1
    eor r2, r2
    eor r3, r3
    push {lr}
    itera:
        cmp r3, r4 /*r4 contiene la cant de caract, r3 es un controlador, cuando r3=r4 significa 
        que ya se acumuló todo*/
        beq convertir
        add r3, #1
        add r0, #1 /*cuenta a la par que r3 solo que tiene un tope, porque son las unidades, va hasta 
        10 y se reinicia, cargando uno en decenas */
        cmp r0, #10
        beq decenas
        cmp r1, #100 /*similar, pero tiene tope en 100, porque es decenas, una vez = a 100, se reinicia 
        a cero y aumenta uno en centenas */
        beq centenas
        b itera

    decenas:
        add r1, #1
        mov r0, #0
        b itera
    centenas:
        add r2, #1
        mov r1, #0
        b itera

    convertir:
        add r2, #0x30 /*empieza por las centenas, las pasa ascii (por el orden de izq a der) */
        strb r2, [r5]
        add r5, #1
        add r1, #0x30
        strb r1, [r5]
        add r5, #1
        bl paridad /*ya que está, chequea paridad, porque tenemos las unidades a mano */
        add r0, #0x30 /*convierte las unidades y guarda todo */
        strb r0, [r5]
        pop {lr}
        bx lr
    .fnend
/*----------------------------------------------------------------- */
paridad:
    /*input r0 --> unidades
    r6 puntero a bitDeParidad */
    /*output: setaer bitDeParidad */
    .fnstart
    cmp r0, #0
    beq setearPar
    cmp r0, #2
    beq setearPar
    cmp r0, #4
    beq setearPar
    cmp r0, #6
    beq setearPar
    cmp r0, #8
    beq setearPar

    setearImpar:
    mov r3, #0x30
    strb r3, [r6]
    b vuelve

    setearPar:
    mov r3, #0x31
    strb r3, [r6]

    vuelve:
    bx lr
    .fnend
/*----------------------------------------------------------------- */
cuentaSeparadores:
    /*input: 
    r1: puntero a inputUsuario
    r2: longitudInputUsuario
    Usa r0: cargar byte; r4: controlador
    output, r5: cuenta separadores --> se carga en cantSeparadores */
    .fnstart
    eor r4, r4
    eor r5, r5
    contar:
    ldrb r0, [r1] 
    cmp r0, #';' 
    addeq r5, #1 
    add r4, #1
    cmp r4, r2
    beq derivar  
    add r1, #1 
    b contar

    derivar:
    cmp r5, #1
    bxne lr
    b extraerPista
    .fnend
/*----------------------------------------------------------------- */
extraerPista:
    .fnstart
        ldr r3, =pista
        ldr r1, =inputUsuario
        eor r0, r0
        eor r4, r4
        buscaSeparador:
        /*para saltear el mensaje e ir directo a la pista*/
        ldrb r0, [r1] 
        cmp r0, #';'
        beq extraeP
        add r1, #1
        b buscaSeparador
        
        extraeP:
        /*hecho para cargar únicamente letras mayúsculas o minúsculas*/
        add r1, #1
        ldrb r0, [r1]
        add r4, #1
        cmp r4, r2
        beq etapaDos
        cmp r0, #'z'
        /* if > */
        bhi extraeP
        cmp r0, #'A'
        /* if < */ 
        bcc extraeP
        cmp r0, #'a'
        bcs cargaP  /* 'a'<=byte<='z' */
        cmp r0, #'Z' 
        bhi extraeP /* if > 'Z' and byte < 'a' */
        /*'A'<=byte<='Z' */
        cargaP:
        str r0, [r3]
        add r3, #1
        b extraeP
    .fnend

extraerMsj:
    .fnstart
    msj:
        ldrb r2, [r1] 
        cmp r2, #';' 
        bxeq lr 
        strb r2, [r3] 
        add r1, #1 
        add r3, #1 
        b msj
    .fnend
/*----------------------------------------------------------------- */
/*match de pista con palabra en msj
Porque fue hecho aderede, pista tiene espacios sólo al final, por lo cual si hay espacio, termina
Las palabras en msj están separadas por cualquier cosa que no es letra (punto, coma, espacio)
Cosas a chequear:
1) que lleguen a espacio (pista) y a una no letra (msj) al mismo tiempo --> si no: pasar a sig palabra
2) que el corrimiento se corresponda byte a byte --> si no: pasar a sig palabra en msj 
(si llega al final del msj y no hay match, error, avisar y termina)
Si pasan 1) y 2) --> MATCH
guarda corrimiento y vuelve a donde lo llamaron*/


/*hay que pasar a otros chars y comparar los corrimientos */
chequearMatch:
    .fnstart
    /*dar como en main 
    ldr r1, =msjInput
    ldr r2, =longitudMsjInput asciz ameoh
    mov r3, #1 /*para controlar final del msj innec asciz ameo
    /*hasta acá, input */

    ldr r0, =pista
    bl obtenerChars /*compara los primeros y devuelve corrimiento en r4*/
    mov r5, r4/*para dejar libre r4 y luego comparar corrimientos*/
    repite:
    add r0, #1
    add r1, #1
    bl obtenerChars
    cmp r3, #0
    beq sinMatch
    cmp r4, r5
    bne pasarPalabra /*r0:=0 (es el de pista), r1 debe volver con la dir de la sig palabra en msj*/
    b repite
    .fnend
/*-------------------------------------*/
/*primer paso: tener los bytes */
obtenerChars:
    .fnstart
    push {lr}
    ldrb r2, [r0]
    ldrb r3, [r1]
    cmp r2, #0 /*porque pista es asciz*/
    beq verPalabra
    eor r4, r4 /*para el corrimiento*/
    bl obtenerCorrimiento 
    pop {lr}
    bx lr  
    .fnend

obtenerCorrimiento:
/*update luego para los números*/
/*r2:=byte de pista; r3:=byte de msj.codificado
r4: guarda el corrimiento */
    .fnstart
    cmp r3, #'z'
    bhi noEsLetra
    cmp r3, #'A'
    bcc noEsLetra
    cmp r3, #'a'
    bcs comparar
    cmp r3, #'Z'
    bhi noEsLetra
    /*llega ya que es 'A'<= byte <='Z'*/

    comparar:
    cmp r2, r3
    bcc restarChars
    addhi r3, #0x1A /*1A cant de letras abecedario, para que de la z vuela a la a*/
    bhi corregir
    bxeq lr

    restarChars:
    sub r4, r3, r2 /*guarda en r4 bpista-bmsj.codificado */
    bx lr  

    corregir:
    sub r4, r3, r2
    bx lr

    noEsLetra:
    bx lr

    .fnend
/*-------------------------------------*/
verPalabra:
    /*para chequear si la palabra también terminó
    se puede afirmar el match porque si llega a este punto, el corrimiento se mantuvo*/
    .fnstart
    cmp r3, #'z'
    /* if > */
    bhi hayMatch
    cmp r3, #'A'
    /* if < */ 
    bcc hayMatch
    cmp r3, #'a'
    bcs pasarPalabra2  /* 'a'<=byte<='z' */
    cmp r3, #'Z'
    bhi hayMatch /* if > 'Z' and byte < 'a' */

    /*si no entra en ninguna, está entre 'A' y 'Z' */
    b pasarPalabra2 

    .fnend
/*-------------------------------------*/
pasarPalabra2:
/*tiene dos partes: 1ro busca que no sea letra y después que sí sea */
    .fnstart
    /*r1 es el puntero de msj, adelantar hasta siguiente palabra*/
    add r1, #1
    ldrb r3, [r1]
    cmp r3, #0
    beq sinMatch
    cmp r3, #'z'
    /* if > */
    bhi buscaPalabra
    cmp r3, #'A'
    /* if < */ 
    bcc buscaPalabra
    cmp r3, #'a'
    bcs pasarPalabra2  /* 'a'<=byte<='z' */
    cmp r3, #'Z'
    bhi buscaPalabra /* if > 'Z' and byte < 'a' */

    b pasarPalabra2
    .fnend


pasarPalabra:
    .fnstart
    /*r1 es el puntero de msj, adelantar hasta siguiente palabra*/
    buscaPalabra:
    add r1, #1
    ldrb r3, [r1]
    cmp r3, #0
    beq sinMatch
    cmp r3, #'z'
    /* if > */
    bhi buscaPalabra
    cmp r3, #'A'
    /* if < */ 
    bcc buscaPalabra
    cmp r3, #'a'
    bcs volverAchequearMatch  /* 'a'<=byte<='z' */
    cmp r3, #'Z'
    bhi buscaPalabra /* if > 'Z' and byte < 'a' */

    volverAchequearMatch:
    b chequearMatch

    .fnend
/*-------------------------------------*/
hayMatch:
    .fnstart
    ldr r4, =corrimiento
    str r5, [r4]
    bal decodificarConPista
    .fnend
/*-------------------------------------*/
sinMatch:
        bl newLine
        ldr r2, =longitudMensajeErrorPista
        ldr r1, =mensajeErrorPista
        bl imprimirString
        bal fin 

/*----------------------------------------------------------------- */


/*----------------------------------------------------------------- */
.global main 
main:
    
    /*Imprimir por pantalla msj que pide input al usuario*/
    ldr r2, =longitudMensaje /*Leer # caracteres*/
    ldr r1, =mensaje   /*Cargamos en r1 la direccion del mensaje*/
    bl imprimirString
bl newLine
bl newLine    

    /*leemos msj*/
    bl leerMensaje

    /*Imprimir por pantalla "Ud. ha ingresado: "*/
    ldr r2, =longitudMensaje1 
    ldr r1, =mensaje1   
    bl imprimirString
    
    ldr r2, =longitudInputUsuario 
    ldr r1, =inputUsuario    
    bl imprimirString

    bl cuentaSeparadores

    bl extraer /*extrae msj, clave, opcion y bit de paridad si corresponde*/
    bl newLine 

    /*Imprimir por pantalla "Mensaje ingresado: "*/
    ldr r2, =longitudMensaje2 
    ldr r1, =mensaje2   
    bl imprimirString

    ldr r2, =longitudMsjInput 
    ldr r1, =msjInput    
    bl imprimirString

    bl newLine

    /*Imprimir por pantalla "Clave ingresada: "*/
    ldr r2, =longitudMensaje3 
    ldr r1, =mensaje3  
    bl imprimirString

    ldr r2, =longitudClave 
    ldr r1, =clave   
    bl imprimirString

    bl newLine

    /*Imprimir por pantalla "Opcion ingresada: "*/
    ldr r2, =longitudMensaje4 
    ldr r1, =mensaje4  
    bl imprimirString

    ldr r2, =longitudOpcion 
    ldr r1, =opcion   
    bl imprimirString

    bl newLine

    /*input para la subrutina claveAdecimal*/
    ldr r1, =clave 
    ldr r3, =claveDecimal 
    mov r4, #10 /*para los pesos */
    eor r0, r0 
    bl claveAdecimal 

    ldr r1, =opcion 
    ldrb r1, [r1] 
    bl chequearOpcion 
    
    /*Prepara para codificar o decodificar*/
    ldr r1, =msjInput 
    ldr r0, =msjCodificado
    ldr r2, =claveDecimal
    ldr r2, [r2]
    eor r5, r5 /*para contar cant de letras procesadas */
    bl filtrarLetras

    ldr r0, =cantCaracteres
    str r5, [r0]

    /*Para contar caracteres y setear bit de paridad*/
    ldr r4, =cantCaracteres
    ldr r4, [r4]
    ldr r5, =cantEnAscii
    ldr r6, =bitDeParidad
    bl enteroAascii

    /*chequear bit y largar error si no corresponde */
    ldr r0, =opcion
    ldrb r0, [r0]
    ldr r1, =bitDeParidad
    ldrb r1, [r1]
    ldr r2, =bitParidadUsuario
    ldrb r2, [r2]
    cmp r0, #'d'
    bne salta
    cmpeq r1, r2
    bne errorBit
    salta:
    cmp r0, #'D'
    bne sigue
    cmp r1, r2
    bne errorBit

    sigue:

    /*Imprimir "Cantidad de caracteres codificados: " */
    ldr r2, =longitudMensaje5
    ldr r1, =mensaje5
    bl imprimirString

   /*Imprimir cantidad de caracteres */
    ldr r2, =longitudCantEnAscii
    ldr r1, =cantEnAscii
    bl imprimirString

    bl newLine    

    /*Imprimir por pantalla "Mensaje codificado: "*/
    ldr r2, =longitudMensaje6 
    ldr r1, =mensaje6  
    bl imprimirString

    /*Imprimir msjCodificado  */
    ldr r2, =longitudMsjCodificado
    ldr r1, =msjCodificado
    bl imprimirString

    bl newLine

    /*Imprimir "Bit de paridad: " */
    ldr r2, =longitudMensaje7
    ldr r1, =mensaje7
    bl imprimirString

    ldr r2, =longitudBitDeParidad
    ldr r1, =bitDeParidad
    bl imprimirString

    /*hacer un salto a fin */
    b fin

    /*si es con pista, salta acá, todo lo anterior se saltea */
    etapaDos:

    bl newLine

    /*Imprimir "Pista ingresada: " */
    ldr r2, =longitudMensaje8
    ldr r1, =mensaje8
    bl imprimirString

    ldr r2, =longitudPista
    ldr r1, =pista
    bl imprimirString

    bl newLine

    /*Input para extraerMsj*/
    ldr r0, =longitudInputUsuario
    ldr r1, =inputUsuario 
    ldr r3, =msjInput
    bl extraerMsj

    /*Imprimir por pantalla "Mensaje ingresado: "*/
    ldr r2, =longitudMensaje2 
    ldr r1, =mensaje2   
    bl imprimirString

    ldr r2, =longitudMsjInput 
    ldr r1, =msjInput    
    bl imprimirString

    bl newLine

    /*parte dudosa, hasta acá todo perfecto */

    /*input para chequearMatch */
    ldr r1, =msjInput
    b chequearMatch

decodificarConPista:
    /*filtrarLetras usa el contenido de opcion, por eso lo cargo acá. Esta función deriva a decodificar*/
    ldr r0, =opcionD
    ldrb r0, [r0]
    ldr r1, =opcion
    strb r0, [r1]
    ldr r0, =msjDecodificado
    ldr r1, =msjInput
    ldr r2, =corrimiento
    ldr r2, [r2]
    eor r5, r5
    bl filtrarLetras

    /*Imprimir "Mensaje: " */
    ldr r2, =longitudMensaje9
    ldr r1, =mensaje9
    bl imprimirString

    ldr r2, =longitudMsjDecodificado 
    ldr r1, =msjDecodificado    
    bl imprimirString

    bl newLine

    ldr r4, =corrimiento
    ldr r4, [r4]
    ldr r5, =cantEnAscii
    ldr r6, =bitDeParidad /*no se usa acá, pero la función lo necesita como input */
    bl enteroAascii

    /*Imprimir "Desplazamiento usado: -" */
    ldr r2, =longitudMensajeDesplazamiento
    ldr r1, =mensajeDesplazamiento
    bl imprimirString

    ldr r2, =longitudCantEnAscii
    ldr r1, =cantEnAscii
    bl imprimirString


fin:
    bl newLine       
    mov r7, #1    
    swi 0


